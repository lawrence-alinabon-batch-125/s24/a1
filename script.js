// Add users

db.users.insertMany(
	[	
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email"	: "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"email"	: "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"email"	: "jfirrelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email"	: "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"email": "dmurphy@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email"	: "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
	]

)

// Add Course

db.courses.insertMany(
	[
		{
			"name": "Professional Development",
			"price": "10000.0"
		},
		{
			"name": "Business Processing",
			"price": "13000.0"
		}
	]
)

// add enrollees od the courses
db.courses.updateOne(
	{
		"name": "Professional Development"
	},
	{
		$set:{
			"enrollees": ["6125b63d0957314bed05548e", "6125b63d0957314bed055490"]
		} 
	}
)

db.courses.updateOne(
	{
		"name": "Business Processing"
	},
	{
		$set:{
			"enrollees": ["6125b63d0957314bed05548f", "6125b63d0957314bed055491"]
		} 
	}
)

//get users who are not administrators

db.users.find(
	{
		"isAdmin" : false
	},
	{
		"firstName": 1, 
		"lastName": 1
	}
)